import * as corpora from "corpora-project";
import * as Discord from "discord.js";
import * as dotenv from "dotenv";
import * as fs from "fs";
import { google, calendar_v3 } from "googleapis";
import * as _ from "lodash";
import * as moment from "moment";
import * as util from "util";

import { ChannelHandler, EventBucketEntry, Game } from "./channel-handler";
import mlb from "./channels/mlb";
import nba from "./channels/nba";
import { getAnnouncement } from "./announcements";

const DEBUG = false;

const SYSOPS: string[] = [ "vogon#9573", "hayley#1111", "jkap#0001" ];
const DEBUGGING_CHANNEL: string = "dev";
const GAME_ANNOUNCE_CHANNEL: string = "other";

function isSysop(username: string, discriminator: string): boolean {
    return SYSOPS.indexOf(`${username}#${discriminator}`) !== -1;
}

const CHANNEL_HANDLERS: Map<string, ChannelHandler<Game>[]> = new Map();

// mount channel objects in the appropriate channels
// todo (vogon): move this out into a configuration file, maybe make it
//     configurable at runtime?
CHANNEL_HANDLERS.set("sports-on-earth-2", [ mlb, nba ]);

dotenv.config();

const TOKEN_PATH = ".google/token.json";

const GCAL_SYNC_RATE_LIMIT = (process.env.RATE_LIMITS === "dev") ? 100 : 1000;

let GOOGLE_AUTHED = false;

function authorize(credentials, callback) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(client_id, client_secret,
        redirect_uris[0]);

    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) {
            console.error(`error loading Google token: ${err}`);
            process.exit(255);
        }

        oAuth2Client.setCredentials(JSON.parse(token.toString()));
        callback(oAuth2Client);
    });
}

interface CalendarSyncState {
    id: string;
    events: calendar_v3.Schema$Event[];
    timeUpdated: number;
    channel: ChannelHandler<Game>;
}

interface CalendarSyncQueueObject {
    id: string;
    channel: ChannelHandler<Game>;
}

const calendars: Map<string, CalendarSyncState> = new Map();
const channelDirty: Map<ChannelHandler<Game>, boolean> = new Map();
const calendarSyncQueue: CalendarSyncQueueObject[] = [];

const pendingGames: Map<ChannelHandler<Game>, Map<string, Game>> = new Map(
    _.map(_.flatten(Array.from(CHANNEL_HANDLERS.values())), ch => [ch, new Map()])
);

async function syncCalendar(gcal: calendar_v3.Calendar,
        queueObj: CalendarSyncQueueObject): Promise<void> {
    // load events that have already happened today, just to make debugging
    // easier
    let midnightToday = moment().startOf('day');

    let events = await gcal.events.list({
        calendarId: queueObj.id,
        timeMin: midnightToday.toISOString(),
        maxResults: 10,
        singleEvents: true,
        orderBy: "startTime"
    });

    calendars.set(queueObj.id, {
        id: queueObj.id,
        channel: queueObj.channel,
        events: events.data.items,
        timeUpdated: Date.now()
    });

    channelDirty.set(queueObj.channel, true);
}

fs.readFile(".google/credentials.json", (err, content) => {
    if (err) {
        console.error(`error loading client secret file: ${err}`);

        // Just don't use google features if no credentials
        return;
    }

    authorize(JSON.parse(content.toString()), (auth) => {
        GOOGLE_AUTHED = true;

        const gcal = google.calendar({ version: "v3", auth });

        // check for stale calendars
        setInterval(() => {
            let now = Date.now();

            CHANNEL_HANDLERS.forEach((ch) => {
                ch.forEach((handler) => {
                    handler.calendarIds.forEach((id) => {
                        let syncState = calendars.get(id);

                        if (!syncState ||
                                (now - syncState.timeUpdated) > 900000) {
                            if (calendarSyncQueue.findIndex((sqo) => sqo.id === id) === -1) {
                                calendarSyncQueue.push({ id, channel: handler });
                            }
                        }
                    });
                });
            });
        }, 5000);

        // update next calendar on sync queue
        setInterval(async () => {
            if (calendarSyncQueue.length > 0) {
                const sqo = calendarSyncQueue.shift();
                process.stdout.write(`syncing calendar "${sqo.id}"... `);

                try {
                    await syncCalendar(gcal, sqo);
                    console.log("ok");
                } catch (e) {
                    console.log(`error: ${e}`);
                }
            }
        }, GCAL_SYNC_RATE_LIMIT);

        // if the sync queue is empty, rebuild every channel's pending event
        // list
        setInterval(() => {
            if (calendarSyncQueue.length > 0) return;

            for (let [channelName, handlers] of CHANNEL_HANDLERS.entries()) {
                for (let handler of handlers) {
                    // if the channel hasn't changed since the last time we ran
                    // through it, skip this whole ordeal
                    if (channelDirty.get(handler) === false) {
                        continue;
                    }

                    let start = Date.now();
                    console.log(`== doing pending event rebuild for "${channelName}" ==`);

                    // bucket all the events for this channel by start time
                    let buckets: { [timestamp: string]: EventBucketEntry[] } = {};

                    for (let calendarId of handler.calendarIds) {
                        let syncState = calendars.get(calendarId);

                        // todo (vogon): this happens at startup; fix this better
                        if (!syncState) {
                            console.log(`${channelName}: missing sync state for ${calendarId}; skipping`);
                            return;
                        }

                        for (let event of syncState.events) {
                            let bucketTimestamp = event.start.dateTime;
                            let bucket = buckets[bucketTimestamp] || [];

                            bucket.push({ calendarId, event });
                            buckets[event.start.dateTime] = bucket;
                        }
                    }

                    // generate game objects
                    for (let timestamp in buckets) {
                        let games = handler.gamesForEventBucket(buckets[timestamp]);

                        games.forEach(g => {
                            if (!pendingGames.get(handler).has(g.pk)) {
                                pendingGames.get(handler).set(g.pk, g);
                            }
                        });
                    }

                    channelDirty.set(handler, false);

                    let end = Date.now();

                    console.log(`== done (in ${end - start}ms) ==`);
                }
            }
        }, 500);

        // announce games that are coming up soon
        setInterval(() => soonPoll(Date.now()), 10000);
    });
});

function soonPoll(when: number) {
    const warningMinutes = 15;

    let start = Date.now();
    let nGames = 0;

    let allTextChannels = client.channels.filter(ch => ch.type === "text") as
        Discord.Collection<string, Discord.TextChannel>;

    for (let [channelName, handlers] of CHANNEL_HANDLERS.entries()) {
        for (let handler of handlers) {
            let allGames = Array.from(pendingGames.get(handler).values());
            nGames += allGames.length;

            let soonGames = _.filter(allGames, g => {
                if (!moment(g.startTime).isBetween(
                    when,   // don't announce games that are already going
                    moment(when).add(warningMinutes, 'minutes')
                            // and don't announce games occurring too far in the future
                )) return false;

                if (g.announced) return false;

                return true;
            });

            if (soonGames.length === 0) continue;

            let channelToAnnounceOn = DEBUG ? DEBUGGING_CHANNEL : channelName;

            handler.announceGames(
                // todo (vogon): make this more robust to multiple guilds +
                // overlapping names
                allTextChannels.find((ch) => ch.name === channelToAnnounceOn),
                soonGames,
                warningMinutes,
                DEBUG,
                false // RIP sports (2000 BC - 2020 AD)
            );
        }
    }

    let end = Date.now();

    console.log(`soon poll done in ${end - start}ms (${nGames} games)`);
}

function sport() {
    // choose a random sport to be playing
    let randomSportIdx = Math.floor(Math.random() * sports["sports"].length);
    let randomSport = sports["sports"][randomSportIdx];

    client.user.setPresence({
        game: { name: randomSport },
        status: "online"
    });

    let allTextChannels = client.channels.filter(ch => ch.type === "text") as
        Discord.Collection<string, Discord.TextChannel>;

    let channelToAnnounceOn = DEBUG ? DEBUGGING_CHANNEL : GAME_ANNOUNCE_CHANNEL;
    let channel = allTextChannels.find((ch) => ch.name === channelToAnnounceOn);

    channel.send(getAnnouncement(randomSport));

    return randomSport;
}

///////////////////////////////////////////////////////////////////////////////
// discord behaviors
///////////////////////////////////////////////////////////////////////////////

const sports = corpora.getFile("sports", "sports");

const client = new Discord.Client();

client.on("ready", () => {
    console.log(`logged in as ${client.user.tag}`);

    // sport up when you start
    sport();
});

client.on("message", (msg: Discord.Message) => {
    let channel = msg.channel as Discord.TextChannel;

    if (isSysop(msg.author.username, msg.author.discriminator)) {
        if (msg.content.startsWith("!test")) {
            if (!GOOGLE_AUTHED) {
                channel.send("google not authed")
            }

            let handlers = CHANNEL_HANDLERS.get(channel.name);
            for (let handler of handlers) {
                // choose a game at random
                let allGames = Array.from(pendingGames.get(handler).values());
                let randomGame = _.sample(allGames);

                console.log(util.inspect(randomGame));

                handler.announceGames(channel, [randomGame], 15, DEBUG, true);
            }

        }

        if (msg.content.startsWith("!soon")) {
            if (!GOOGLE_AUTHED) {
                channel.send("google not authed")
            }

            let args = msg.content.split(" ");
            let when = parseInt(args[1]);

            if (!isNaN(when)) {
                soonPoll(when);
            }
        }

        if (msg.content.startsWith("!sport")) {
            sport();
        }
    }
})

// Every 120 minutes
setInterval(sport, 120 * 60 * 1000);

client.login(process.env.DISCORD_TOKEN);
